#! /usr/bin/env python3

#Script that takes the raw data as input, processes it and outputs clean data. We chose to write functions that return dataframes instead of only writing to files, to allow for more flexibility (can be used in scripts, and saving to disk is a one-liner).

import itertools, os
import pandas as pd
from string import capwords

stats_header = ['Name','Games','Sets','Points','W-L','Break Points','Serves','Aces','Serve errors','Aces per set','Serve efficiency','Passes','Passes--','Passes-','Passes++','Passing efficiency','Passing efficacy','Attacks','Other attacking errors','Blocked','Attacks++','Attacking efficiency','Attacking efficacy','Blocks','Blocks per set']
athlete_header = ['Game','Sets','Points','W-L','Break Points','Serves','Aces','Serve errors','Aces per set','Serve efficiency','Passes','Passes--','Passes-','Passes++','Passing efficiency','Passing efficacy','Attacks','Other attacking errors','Blocked','Attacks++','Attacking efficiency','Attacking efficacy','Blocks','Blocks per set']

games_index = pd.Index(list(range(27)) + ['Totals','Per set'], name="Games")

#Function to extract the game number fron a string of the form "RSA1 - N ROUND" where ROUND can be either Andata or Ritorno, in the latter case the game number has to be increased by 13 (the games in a round)
def game_number(string):
    string = string.replace("RSA1 - ","")
    words = string.split(" ")
    if len(words) == 2:
        if words[1] == 'Andata':
            return int(words[0])
        else:
            return int(words[0])+13
    else:
        #if the string is of some other kind we leave it untouched
        return string


#Function that reads the raw season data and returns a clean dataframe
def clean_season_data(role,year,folder='./'):
    #We expect year to be string or integer
    #Filename is constructed from role and year only.
    filename = folder + str(year) + role + '.csv'
    df = pd.read_csv(filename,header=None) #load the raw data. There is no header, all the rows represent data
    df.columns = stats_header #we set the header

    #Some columns can be computed from the others, so we drop them. We only keep the independent data. Attacking efficiency and efficacy, blocks per set, aces per set, serve efficiency, passing efficiency and efficacy can all be computed from the rest
    df = df.drop(columns=['Aces per set','Serve efficiency','Passing efficiency','Passing efficacy','Attacking efficiency','Attacking efficacy','Blocks per set'])

    #The original data has rows only for players that played at least one game. The NaN values represent no recorded plays (e.g. 0 blocks or 0 passes) . The correct interpretation is for them to be zero.
    df = df.fillna(0)

    #The remaining columns are all counting stats that come from the games, plus the player's name. Therefore all the numerical stats must be integers, as players can only perform whole technical moves. We then recast all the numerical values as integers.
    df.loc[:, df.columns != 'Name'] = df.loc[:, df.columns != 'Name'].astype('int')
    return df


#Function that reads the raw data of a single player and returns a clean dataframe
def clean_athlete_data(full_name,year,folder='./'):
    #We expect year to be string or integer
    #Filename is constructed from full name and year only. We capitalize the full name and remove white spaces (we use capwords to avoid problems with apostrophes in foreign names as much as possible
    filename = folder + capwords(full_name).replace(' ','') + str(year) + '.csv'
    df = pd.read_csv(filename,header=None) #load the raw data. There is no header
    df.columns = athlete_header #set the header

    #Some columns can be computed from the others, so we drop them. We only keep the independent data. Attacking efficiency and efficacy, blocks per set, aces per set, serve efficiency, passing efficiency and efficacy can all be computed from the rest
    df = df.drop(columns=['Aces per set','Serve efficiency','Passing efficiency','Passing efficacy','Attacking efficiency','Attacking efficacy','Blocks per set'])

    #The original data only has rows for the games in which the player actually played. The NaN values represent no recorded plays (e.g. 0 blocks), but only if the player played in that game. So we can fill them as zero without risking to attribute all-zeros statline to the player for games he did not play.
    df = df.fillna(0)

    #The remaining columns are all counting stats and we filled NaN as zeros so we could recast as int. But it is likely we will need the float data type anyway (see below), so we avoid this
    #df.loc[:, df.columns != 'Name'] = df.loc[:, df.columns != 'Name'].astype('int')

    #process the column "Game" to display game numbers and use it as index
    df['Game'] = df['Game'].map(lambda x: game_number(x))
    df = df.set_index('Game')

    #the original data contains one row for every played game + two additional rows: one computing the totals and one computing the average per game of every stat. Since these are easily computable, we drop the rows
    df = df.drop(labels=['Totali','Medie a Partita'])

    #give to the set of values the name of the player
    df.columns.name = full_name

    #fill the dataframe, adding empty rows for the games not already appearing (because the player did not play). We do this by reindexing agains the list of all games numbers. The new rows will only have NaN values
    #As NaN is of float type, this causes the dataframe to be recast in the float data type, if any row is added. This is the reason we did not cast everything as integer even though it'd be possible, in many cases the dataframe will come back to float anyway.
    df = df.reindex(games_index)
    return df


#When called directly, the script cleans raw data and saves it into data files
if __name__=='__main__':
    DATA_FOLDER = '../data/' #location where data files are

    #Years and roles for which we clean the data
    CURRENT_YEAR = 2020
    historic_year = list(range(2017,CURRENT_YEAR))
    roles = ['middles','libero','setters','spikers']

    #Cleaning of current year data. This data is assumed to change at every run, so we always process it
    for role in roles:
        clean_season_data(role,CURRENT_YEAR,DATA_FOLDER).to_csv(DATA_FOLDER + str(CURRENT_YEAR)+role+'-clean.csv',index=False,encoding='utf-8')
        print("Cleaned",role,CURRENT_YEAR)

    #Cleaning of past years data. This data does not change, so if the processed files already exists, we do not produce them again
    for (role,year) in itertools.product(roles,historic_year):
        filename = DATA_FOLDER + str(year) + role + '-clean.csv'
        if not os.path.isfile(filename):
            clean_season_data(role,year,DATA_FOLDER).to_csv(DATA_FOLDER + str(year)+role+'-clean.csv',index=False,encoding='utf-8')
            print("Cleaned",role,year)

#! /usr/bin/env python3

#Script that scrapes the raw data from legavolley.it and saves it into CSV files

import itertools, os
import requests
import pandas as pd


#Function to retrieve the snippets dictionary. We use as a dictionary the HTML page where we can select the player in our browser. Here the snippets are the values of some HTML tag that contains the full name.
def get_snippets_dictionary():
    #Since the dictionary is immutable, we save it to a file to avoid issuing a new request every time. We first try to load the file with the dictionary
    try:
        disk_file = open('snippets_dictionary.txt','r')
        snippets_dictionary = disk.file.read()
        disk_file.close()

    #if loading the file fails, the method 'open' throws an error, so we resort to getting the page from the server, and then save it to a file. We save in txt because saving in HTML loses some tags, in particular those that we need
    except:
        page = requests.get('http://www.legavolley.it/statistiche/?TipoStat=2.2&Serie=1&AnnoInizio=2020&Fase=1&Giornata=0&Atleta=')
        snippets_dictionary = str(page.content)
        with open('snippets_dictionary.txt','w') as disk_file:
            disk_file.write(snippets_dictionary)

    return snippets_dictionary

def get_player_snippet(full_name):
    snippets_dictionary = get_snippets_dictionary()
    index = snippets_dictionary.find(full_name) #we search for the full name in the snippets dictionary. The function .find returns the index at which the string begins. The full name has the strings that we have to attach to the URL as the values that get POSTed when we select the player in the menu. I.e. there is an HTML tag with key=something and value=player_snippet in the page and then in the dictionary
    player_snippet = snippets_dictionary[(index-11):(index-1)] #we get the string to attach to the URL: it is just before the name of the player, as the "value" field of the tag
    return player_snippet


def season_stats_to_csv(role,year,folder='./'):
    base_url = 'http://www.legavolley.it/statistiche/?TipoStat=2.4&Serie=1&AnnoInizio=' #first part of the URL
    mid_url = '&Fase=1&AnnoFine=' #middle part of the URL: indicates that we are looking for regular season games
    season_url = base_url + str(year) + mid_url + str(year) + roles[role] #this is the URL where we find the cumulative season data

    #Extract the content with pandas read_html. The attrs parameter specifies the tag values that the objects must have. The first two rows are just headers which we discard.
    html_tables = pd.read_html(season_url,attrs = {'id': 'Statistica'},skiprows=[0,1],decimal=',',thousands='.')

    #There are two tables with the same ID, the first one has the content, the second one the legend
    stats_table = html_tables[0]
    stats_table.to_csv(folder + str(year) + role + '.csv',index=False,header=False,encoding='utf-8')

def athlete_stats_to_csv(full_name,year,folder='./'):
    #We expect year to be string or integer
    base_url = 'http://www.legavolley.it/statistiche/?TipoStat=2.2&Serie=1&AnnoInizio=' #first part of the URL
    mid_url = '&Fase=1&Giornata=0&Atleta=' #middle part of the URL: indicates that we are spanning all the games
    player_snippet = get_player_snippet(full_name)

    player_url = base_url + str(year) + mid_url + player_snippet #this is the url at which we find the player's data

    #read_html returns a list of dataframes, one for every table in the html. In our case the relevant one is the first (it used to be the second until May 2020). To parse correctly we use comma as decimal since it's European data (we need to set thousands to the dot for this to work). The first two rows are just headers which we discard. Finally, the first column is saying the game number, and we use it as a label
    dataframe = pd.read_html(player_url,flavor='html5lib',skiprows=[0,1],index_col=0,decimal=',',thousands='.')[0]
    dataframe.to_csv(folder + full_name.replace(' ','') + str(year) + '.csv',header=False,encoding='utf-8')


if __name__=='__main__':
    DATA_FOLDER = '../data/' #location where we save data files

    #Years to retrieve statistics for, up to the last one. Statistics for the season 2019/2020 are listed under year 2019
    CURRENT_YEAR = 2020
    historic_years = list(range(2017,CURRENT_YEAR))

    #URL snippets associated to the roles
    roles = {'middles': '&Ruolo=1',
    'libero': '&Ruolo=2',
    'setters': '&Ruolo=3',
    'spikers': '&Ruolo=4'}

    #Scraping of the statistics for the current year. This data changes continuously so we scrape it at every run
    for role in roles:
        season_stats_to_csv(role,CURRENT_YEAR,DATA_FOLDER)
        print("Scraped",role,CURRENT_YEAR)

    #Scraping of the statistics for the previous years. As the data does not change, if we already have a CSV file for these, we skip the process
    for (role,year) in itertools.product(roles,historic_years):
        filename = DATA_FOLDER + str(year) + role + '.csv'
        if not os.path.isfile(filename):
            season_stats_to_csv(role,year,DATA_FOLDER)
            print("Scraped",role,year)

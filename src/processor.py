#! /usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import requests

import draw_functs
from scraping_functs import athlete_to_dataframe

sns.set(style='darkgrid')

#Function to extract the surname from a full name (approximately). This is quick but not perfect for full names with more than 2 words: in general they are difficult to deal with, as the middle name can be a second name, part of the surname of something else.
def format_names(name):
    words = name.split()
    if len(words) == 2:
        return words[0]
    else:
        return ' '.join(words[0:-1]) #list[n:m] goes from index n to m-1


#Headers that we want to set (in English)
names = ['Name','Games','Sets','Points','W-L','Break Points','Serves','Aces','Serve errors','Aces per set','Serve efficiency','Passes','Passes--','Passes-','Passes++','Passing efficiency','Passing efficacy','Attacks','Other attacking errors','Blocked','Attacks++','Attacking efficiency','Attacking efficacy','Blocks','Blocks per set']

#Import the csv in pandas using names as headers, and using the first column as data instead of indices
files = ['2020middles.csv','2020setters.csv','2020libero.csv','2020spikers.csv']
roles = ['middles','setters','libero','spikers']
dataframes = {}
for (data,role) in zip(files,roles):
    with open(data,'r') as datafile:
        df = pd.read_csv(datafile,names=names,index_col=None)
        dataframes[role] = df


### Dataset preparation

#We filters spikers with at least 3 attacks and keep only the features we want to plot.
#copy() is to avoid SettingWithCopyWarning, since we cannot be sure about the behaviour and we do not want to modify the original dataframe, we explicitly make a copy (compared to a view)
df_graphs = dataframes['spikers'].loc[dataframes['spikers']['Attacks'] > 3,['Name','Attacks','Attacking efficiency','Other attacking errors','Blocked']].copy()

#Format the column "Name" of the dataframe to show surnames
df_graphs.loc[:,'Name'] = df_graphs.loc[:,'Name'].map(lambda x: format_names(x))

#Since everyone in the DF has attacking efficiency > 30, we scale everything down to make the graph nicer
#df_graphs.loc[:,'Attacking efficiency'] = df_graphs.loc[:,'Attacking efficiency'].map(lambda x: x-30)

#Sorts the dataframe descending according to the values in the column "Attacking efficiency" (% of succesful attacks)
df_graphs = df_graphs.sort_values(by='Attacking efficiency',ascending=False)

#Compute the total number of errors
df_graphs['Attacking errors'] = df_graphs['Other attacking errors'] + df_graphs['Blocked']


### First graph: bar plot to show who's attacking with higher percentages. On the x-axis we put the scale: the first range are the positions, the second the values to print (remember we translated everything down by 3). We already processed the labels to put on the y-axis (surnames), so we adjust size only

draw_functs.draw_barplot(df_graphs)

### Second graph: stacked bar plot showing who's making more errors and how they are divided between blocked and other errors. On the x-axis we put the surnames, while on the y-axis we put the number of errors, with a tick every 10 errors.

df_graphs = df_graphs.sort_values(by='Attacking errors',ascending=False)
draw_functs.draw_stackedplot(df_graphs)



#We now try to do a deeper analysis for the top 10 scorers. Find the rows corresponding to the 5 top scorers
top_scorers = dataframes['spikers'].sort_values(by='Attacks++',ascending=False).iloc[:10,:]


#We get the initial page, where hidden in the HTML tags there are the strings used to identify the players
base_url = 'http://www.legavolley.it/statistiche/?TipoStat=2.2&Serie=1&AnnoInizio=2020&Fase=1&Giornata=0&Atleta='
base_page = requests.get(base_url)
base_content = str(base_page.content)
dframes = []

#This is the list of indices we want to use, i.e. game numbers, totals, per set values.
new_index = pd.Index([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,"Totals","Per set"],name="Games")

#For every player we have to construct the string used in the legavolley database, which for John Does born in 1995 is JOH-DOE-95. We extract the birth year from the list of options in the drop-down list in the legavolley website (the options are written in the html). Then we get the data, parse it and process it.
for full_name in top_scorers['Name']:
    dframes.append(athlete_to_dataframe(full_name,new_index,names,base_url,base_content))

max_game = 1 #hard coded for now

#we want to concatenate dataframes. They have the same columns so concatenating will just put together the rows. But we have to keep track from which dataframe they came, so we use 'player' as a flag, displaying the surname
for dframe in dframes:
    dframe['player'] = format_names(dframe.columns.name)

#Concatenate and drop the rows corresponding to totals and per set.
df_concat = pd.concat(dframes)
df_concat = df_concat.drop(index=['Totals','Per set'])


### Third graph: a box plot with data points, to show distribution of the succesful attacks across the games. We plot the boxes side by side to compare the distributions.

draw_functs.draw_boxplot(df_concat)

### Fourth graph: evolution over time of the cumulative successful attacks. We put all plots in a single figure for comparison.

draw_functs.draw_evolution(dframes,max_game)



### Analysis of spikers + libero passing

#We prepare the dataframe by joining spikers and libero with enough passes
df_spikers = dataframes['spikers'][dataframes['spikers'].loc[:,'Passes']>5].copy()
df_libero = dataframes['libero'][dataframes['libero'].loc[:,'Passes']>5].copy()
df_spikers["Role"] = "Spiker"
df_libero["Role"] = "Libero"
df_passing = pd.concat([df_spikers,df_libero])

### Graph: distribution of passing efficiency

draw_functs.draw_dist(df_passing,'Passing efficiency')

### Graph: average passing efficiency, grouping by roles first. In this way we can compare which role is on average the better passer (libero expected)

draw_functs.draw_barplot_grouping(df_passing,["Role"],"Passing efficiency")

### Graph: we want to investigate the relation between passing efficiency and volume of passes. On one hand, worse passers are more targeted during the service, so they should pass more. On the other hand, better passers cover more space when receiving, with worse passers getting a smaller area to cover, so these last shoud pass less. Which of the two motivation is more relevant, if any? Is there a trend?

draw_functs.draw_scatterplot(df_passing,"Passing efficiency","Passes","Role")


### Analysis of middle blockers

#We prepare the dataframe by choosing middle blockers with at least max_game sets played. We only keep the relevant features.
df_middles = dataframes['middles'][dataframes['middles'].loc[:,'Sets']>=max_game].copy()
df_middles = df_middles.drop(columns=['Passes','Passes--','Passes-','Passes++','Passing efficiency','Passing efficacy'])
df_middles['Attacks++ per set'] = df_middles['Attacks++']/df_middles['Sets']
df_middles['Attacks per set'] = df_middles['Attacks']/df_middles['Sets']

#Our first aim is to try to check if we can efficiently classify middle blockers in three categories: good at blocking, good at spiking, good at both. We leave also open the possibility of a fourth category (not good) hoping that noone will fall into it. First of all, we plot the data.

draw_functs.draw_middles_scatter(df_middles,"Attacking efficiency","Blocks per set")

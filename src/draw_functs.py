import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

def draw_barplot(df):
    fig1 = plt.figure()
    sns.barplot(x='Attacking efficiency',y='Name',data=df,color='b')
    plt.xticks(size='small')
    plt.yticks(size='xx-small')
    plt.title('A1 Spikers\' efficiency')
    plt.xlabel('Attacking efficiency %')
    fig1.savefig('fig1.pdf',bbox_inches='tight')


def draw_stackedplot(df):
    fig2 = plt.figure()

    #Seaborn does not include an interface for stacked graphs, so we just use the "plot" interface of pandas (is a wrapper for matplotlip)
    plot = df.loc[:,['Name','Other attacking errors','Blocked']].plot.bar(x='Name',stacked=True)

    #pandas' plot does not automatically put the result in the figure environment, so we have to catch it
    fig2 = plot.get_figure()
    plt.xticks(rotation=55,ha='right',size='xx-small')
    plt.title('A1 Spikers\' total attack errors')
    plt.yticks(np.arange(0,151,10),np.arange(0,151,10))
    fig2.savefig('fig2.pdf',bbox_inches='tight')


def draw_boxplot(df):
    fig3 = plt.figure()
    
    #swarmplot plots the dot for every value vertically. We do it on top of a box plot so that we can show all the data inside their boxes
    sns.boxplot(x='player',y='Attacks++',data=df)
    sns.swarmplot(x='player',y='Attacks++',data=df,color=".2")
    plt.xticks(rotation=65)
    plt.title('Distribution of top scorers\' successful attacks')
    plt.xlabel('Name')
    fig3.savefig("fig3.pdf",bbox_inches='tight')



def draw_evolution(dframes,max_game):
    fig4 = plt.figure()
    for dframe in dframes:
        dframe['Attacks-evo'] = dframe['Attacks++'].cumsum()
        sns.lineplot(x=np.arange(1,27), y='Attacks-evo',data=dframe.iloc[0:-2,:],label=dframe.columns.name)
    plt.title('Evolution over time of top scorers\' total points')
    plt.xticks(ticks=np.arange(1,27),labels=np.arange(1,27))
    plt.xlim(1,max_game) #if we plot all the games, those not yet played display a useless (and false) horizontal line, so we cut the graph.
    fig4.savefig('fig4.pdf',bbox_inches='tight')


def draw_dist(df,feature,name='fig5.pdf'):
    fig5 = plt.figure()
    sns.distplot(df[feature],bins=np.arange(0,50,5),kde=False)
    plt.yticks(ticks=np.arange(0,20),labels=np.arange(0,20))
    plt.xticks(ticks=np.arange(0,50,5),labels=np.arange(0,50,5))
    plt.title('Distribution of passing efficiency')
    fig5.savefig(name)


def draw_barplot_grouping(df,grouping_by,feature):
    fig = plt.figure()
    plat = df.groupby(grouping_by).mean().plot(y=feature,kind="bar",legend=None)
    fig = plat.get_figure()
    plt.xticks(rotation=0)
    plt.ylabel(feature)
    plt.title("Average passing efficiency by role")
    fig.savefig("fig6.pdf")


def draw_scatterplot(df,xvar,yvar,hue=None):
    fig = plt.figure()
    sns.scatterplot(data=df,x=xvar,y=yvar,hue=hue)
    plt.title('Passers: Total passes vs Passing efficiency')
    fig.savefig("fig7.pdf")


def draw_middles_scatter(df,xvar,yvar):
    fig = plt.figure()
    sns.scatterplot(data=df,x=xvar,y=yvar)
    plt.title('Middle Blockers: Blocks per set vs Attacking efficiency')
    fig.savefig("fig8.pdf")

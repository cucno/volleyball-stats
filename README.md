# Volleyball Analytics #

The aim of this project is to create volleyball statistics analytics with Python. It focuses on the top Italian league (Superlega).

I created this project because, as a huge fan of volleyball, I feel that in this sport analytics are used less than in others. If insiders may use them, they are not available to the general public, and only basic statistics are quoted in volleyball news and articles. With this project I am trying to address the following points:
 - Creating some easy-to-digest, simple charts to visualise common interesting statistics
 - Trying to find trends and correlations through data analysis
 - Potentially, finding new analytics or statistics that can give new insights or help better understand the game
 - Automating the process to run periodically

The current version contains:
 1. Scraping and cleaning the data
 2. Plotting of charts on spiking: efficiency, errors distribution
 3. Analysis of the five top scorers of the league: chart with distribution of successful attacks across the games, evolution over time of cumulative successful attacks
 4. Analysis of passing: efficiency, average efficiency by role (spikers vs libero), relation between efficiency and volume

Future ideas: categorisation of middle blockers into good at blocking/attacking/both. Comparison with past data. Combination of biometric data. Inclusion of data about single matches and performances.

#### Coverage ####

The analysis started during the 2019-2020 season. However, due to the Covid-19 pandemic, that season was suspended and then ceased without further games.
It is now covering the current 2020-2021 season.

## Code ##

The folder `src` contains all the code. The file [scraper.py](scraper.py) is the main scraping file, it scrapes the data from the Italian volleyball league website. This code formats the data into CSV files, which are produced at every push, and available as Artifacts.

The file [processor.py](processor.py) cleans and formats the data produced by the scraper. Then it produces the charts. The plotting functions are written separately in the file [draw_functs.py](draw_functs.py). All the charts are available as PDF files as Artifacts.

## Data ##

The data is currently available in CSV files as Artifacts, produced with Gitlab Jobs. In the future they will likely be put in the main project files, for persistency and to avoid collecting the data every time. The files have the following name scheme: "year"+"role".csv. For example `2019setters.csv` contains data on setters for the 2018-2019 season.

## Technologies ##

Python 3.7.3, Numpy 1.16.2, Pandas 0.23.3, Matplotlib 3.0.2, Seaborn 0.9.0, BeautifulSoup from bs4 0.0.1, Requests 2.21.0

## Data source and licensing ##

The data is taken from the Italian volleyball league website, in the "Statistiche" section:

https://www.legavolley.it/statistiche/?TipoStat=2.4&Serie=1

At every run the data is scraped again in order to be as updated as possible.
